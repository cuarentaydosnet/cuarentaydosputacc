const argon2 = require('argon2');
const requestJson = require('request-json');
const uuid = require('uuid/v4');

exports.handler = function(event,context,callback){
    console.log("PUT account");
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    
    let body = JSON.parse(event.body);
    var httpClient = requestJson.createClient(db);
    var cambiaalias = '{ "$set" :{ "alias" : "' + body.alias + '" }}';
    var cambiaactivo = '{ "$set" :{ "activo" : ' + body.activo + ' }}';

    console.log("DBUG")
    console.log(cambiaalias)
    console.log(cambiaactivo)
    console.log(event.pathParameters.accountid)
    httpClient.put("account/" + event.pathParameters.accountid + "?" + apikey, JSON.parse(cambiaalias), function(err,resMLab,body){
        if(err){
            var response = {
                "statusCode" : 501,
                "body": JSON.stringify(err),
            }
            return callback(null,response);
        }else{
            httpClient.put("account/" + event.pathParameters.accountid + "?" + apikey, JSON.parse(cambiaactivo), function(err,resMLab,body){
                if(err){
                    var response = {
                        "statusCode" : 501,
                        "body": JSON.stringify(err),
                    }
                } else {
                    var response = { 
                        "statusCode": 200,
                        "headers" : {
                            "Access-Control-Allow-Origin": "*"
                        },                        
                        "body": JSON.stringify(body._id),
                        "isBase64Encoded": false
                    };
                }
                return callback(null,response);
             }); 
        }
    });
}


